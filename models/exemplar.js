var mongoose = require('mongoose');
var configDB = require('../config/database.js');

console.log('configDB.url:' + configDB.url)
mongoose.connect(configDB.url, function(err) {
	if (err)
		throw err
	else
		console.log('deu certo')	
})

var Schema = mongoose.Schema;
var ExemplarSchema = Schema({
	livro: {type: Schema.ObjectId, ref: 'Livro', required: true},
	patrimonio : {type: String, required: true, unique:true},
	locador: {type: Schema.ObjectId, ref: 'User'},
	locatario: {type: Schema.ObjectId, ref: 'User'},
	dataReserva:{ type: Date, required: function(){return this.status=='Reservado'} },
	dataCancelamento:{ type: Date, required: function(){return this.status=='Reserva Cancelada'}},
	dataAluguel:{ type: Date, required: function(){return this.status=='Alugado'}},
	dataDevolucao:{ type: Date, required: function(){return this.status=='Devolvido'}},
	status: {type: String, required: true , enum:['Disponivel', 'Reservado', 'Reserva Cancelada', 'Alugado', 'Devolvido']},
	historico: [{type: Schema.ObjectId, ref: 'Aluguel', required: false}]
})



//Export model
module.exports = mongoose.model('Exemplar', ExemplarSchema);