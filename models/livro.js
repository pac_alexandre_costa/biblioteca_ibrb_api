var mongoose = require('mongoose');
var configDB = require('../config/database.js');

console.log('configDB.url:' + configDB.url)
mongoose.connect(configDB.url, function(err) {
	if (err)
		throw err
	else
		console.log('deu certo')
})

var Schema = mongoose.Schema;
var LivroSchema = Schema({
	isbn: {type: String, required: false, unique: false},
	titulo: {type: String, required: true},
	descricao :{type: String},
	ano:{ type: Number},
	capa: {type: String},
	imagem : {type: String},
	autor: [{type: Schema.ObjectId, ref: 'Autor', required: true}],
	comentario : [{type: Schema.ObjectId , ref:'Comentario'}],
	visualizacoes : {type:Number}
})

LivroSchema.index({'titulo' : 'text' })



//Export model
module.exports = mongoose.model('Livro', LivroSchema);
