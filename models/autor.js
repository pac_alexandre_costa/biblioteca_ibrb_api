var mongoose = require('mongoose');
var configDB = require('../config/database.js');

console.log('configDB.url:' + configDB.url)
mongoose.connect(configDB.url, function(err) {
    if (err)
     throw err
    else
      console.log('deu certo')
})

var Schema = mongoose.Schema;
var AutorSchema = Schema(
  {
    nome: {type: String, required: true, max: 100}
  }
);
AutorSchema.index({'nome':'text'});

//Export model
module.exports = mongoose.model('Autor', AutorSchema);
