angular.
module('autorEdit').
component('autorEdit',{
	templateUrl : 'autor-edit/autor-edit.template.html',
	controller: ['AutorService', '$routeParams', '$location', function(AutorService, $routeParams, $location){
		var self = this

		self.autor = AutorService.get({id: $routeParams.autorId})
		self.update = function(){
			console.log('funcao update foi chamada', self.autor.nome)
			AutorService.update({id:self.autor.id},
				self.autor,
				(data)=>{$location.path('/autores').replace();},
				(err)=>{self.msgErr = req.data.message; console.log('deu ruim na edicao do autor')}
			)
		}
	}]
})