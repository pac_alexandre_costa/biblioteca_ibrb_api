angular.
module('navbar').
component('navbar', {
	templateUrl: 'navbar/navbar.template.html',
	controller: ['AuthService', function(AuthService){
		var self=this;
		
		self.gerenciamentoPermission=false;
		if (AuthService.getUser().perfil==='admin'){
			self.gerenciamentoPermission=true
		}
		
	}]
})