angular.
module('livroDetail').
component('livroDetail',{
    templateUrl: 'livro-detail/livro-detail.template.html',
    controller: ['LivroService',  'AluguelService', '$routeParams', 'AuthService', 'ExemplarService', 'UserService', function(LivroService, AluguelService, $routeParams, AuthService, ExemplarService, UserService){
        var self = this;
        self.usuario = AuthService.getUser()
        self.reservarPermission=false;
        self.cancelarPermission=false;
        self.alugarPermission=false;
        self.devolverPermission=false;


        self.editPermission=false;
        if(AuthService.getUser().perfil==='admin'){
            self.reservarPermission=true;
            self.cancelarPermission=true;
            self.alugarPermission=true;
            self.devolverPermission=true;
        }else if(AuthService.getUser().perfil==='usuario'){
            self.reservarPermission=true;
            self.cancelarPermission=true;
            self.alugarPermission=false;
            self.devolverPermission=false;
        }


        if (AuthService.getUser().perfil==='admin'){
            UserService.query(
                (data)=>{self.usuarios = data},
                (error) => {console.log('deu ruim ao recuperar os usuarios ....', error);}
                )
        }


        LivroService.get({id: $routeParams.livroId}, 
            (livroDoc)=>{
                self.livro = livroDoc;
            },
            (err)=>{console.log('deu ruim galera', err); self.msgErr = err.data.message}
            )
        this.reservar = function(exemplar){
            console.log('function RESERVAR foi chamada', exemplar);
            ExemplarService.reservar({id:exemplar._id}, 
                (data)=>{self.livro = LivroService.get({id:self.livro._id});},
                (err)=>{console.log('deu ruim na reserva do exemplar', err);self.msgErr = err.data.message}
                )
        }		
      /*  this.cancelar = function(exemplar){
            console.log('function cancelar foi chamada', exemplar);
            ExemplarService.cancelar({id:exemplar._id}, 
                (data)=>{self.livro = LivroService.get({id: self.livro._id});},
                (err)=>{console.log('deu ruim no cancelamento do exemplar', err)}
                )
        }		
        this.alugar = function(exemplar){
            console.log('function alugar foi chamada', exemplar);
            ExemplarService.alugar({id:exemplar._id}, 
                (data)=>{self.livro = LivroService.get({id: self.livro._id});},
                (err)=>{console.log('deu ruim no aluguel do exemplar', err)}
                )
        }		
        this.devolver = function(exemplar){
            console.log('function devolver foi chamada', exemplar);
            ExemplarService.devolver({id:exemplar._id}, 
                (data)=>{self.livro = LivroService.get({id: self.livro._id});},
                (err)=>{console.log('deu ruim na devolucao do exemplar', err)}
                )
            }	*/	

        }]
    })