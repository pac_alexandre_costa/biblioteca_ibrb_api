angular.
module('gerenciamento').
component('gerenciamento',{
  templateUrl: 'gerenciamento/gerenciamento.template.html',
  controller : [ '$http', '$location', 'ExemplarService', 'LivroService', 'UserService', 'AuthService', function AluguelController( $http, $location, ExemplarService, LivroService, UserService, AuthService){
    console.log('estou no controlador do gerenciamento');
    var self=this;
    console.log('estou no controlador do gerenciamento')


    self.cancelar = function(exemplar){
      ExemplarService.cancelar({id:exemplar._id}, 
        (data)=>{self.obterEmprestimos();},
        (err)=>{console.log('deu ruim na reserva do exemplar', err)}
        )
    };

    self.confirmar = function(exemplar){
      ExemplarService.confirmar({id:exemplar._id}, 
        (data)=>{self.obterEmprestimos();},
        (err)=>{console.log('deu ruim na reserva do exemplar', err)}
        )
    };

    self.devolver = function(exemplar){
      ExemplarService.devolver({id:exemplar._id}, 
        (data)=>{self.obterEmprestimos();},
        (err)=>{console.log('deu ruim na reserva do exemplar', err)}
        )
    };

    self.obterEmprestimos=function(){
      ExemplarService.emprestimos(
        (data)=>{self.emprestimos = data;},
        (error)=>{console.log('deu ruim ao recuperar os historicos', error);}
        );
    }

    self.obterEmprestimos();

  }]
})