angular.
module('core').
factory('LivroService', [ '$resource', 'CONFIG', function($resource, CONFIG){
    return $resource( CONFIG.URL_API + '/livro/:id', {} , {
        'update': { method : 'PUT' }
    })
}]).
factory('AutorService', ['$resource', 'CONFIG', function($resource, CONFIG){
    return $resource( CONFIG.URL_API + '/autor/:id', {} , {
        'update': { method : 'PUT' },
        'livros':{
            url: CONFIG.URL_API + '/autor/:id/livros',
            method: 'GET',
            isArray: true
        }

    })
}]).
factory('LoginService', ['$resource', 'CONFIG', function($resource, CONFIG){
    console.log('content of CONFIG ', CONFIG)
    return $resource( CONFIG.URL_API + '/account/login', {}, {})
}]).
factory('UserService', ['$resource', 'CONFIG', function($resource, CONFIG){
    console.log('content of CONFIG ', CONFIG)
    return $resource( CONFIG.URL_API + '/user/:id', {}, {
        updatePassword:{
            url: CONFIG.URL_API+'/user/updatePassword',
            method: 'POST'}
        })
}]).
factory('AccountService', ['$resource', 'CONFIG', function($resource, CONFIG){
    console.log('content of CONFIG ', CONFIG)
    return $resource( CONFIG.URL_API + '/account/signup/:id', {}, {})
}]).
factory('AluguelService', ['$resource', 'CONFIG', function($resource, CONFIG){
    return $resource( CONFIG.URL_API + '/aluguel/:id',{},{
        reservar:{
            url: CONFIG.URL_API + '/aluguel/reservar',
            method: 'POST'
        },
        cancelar:{
            url: CONFIG.URL_API + '/aluguel/cancelar',
            method: 'POST'
        },
        alugar:{
            url: CONFIG.URL_API + '/aluguel/alugar',
            method: 'POST'
        },
        devolver:{
            url: CONFIG.URL_API + '/aluguel/devolver',
            method: 'POST'
        }
    })
}])
.
factory('ExemplarService', ['$resource', 'CONFIG', function($resource, CONFIG){
    return $resource( CONFIG.URL_API + '/exemplar/:id',{id:'@id', acao: '@acao'},{
        reservar:{
            url: CONFIG.URL_API + '/exemplar/:id/reservar',
            method: 'POST'
        },
        cancelar:{
            url: CONFIG.URL_API + '/exemplar/:id/cancelar',
            method: 'POST'
        },
        confirmar:{
            url: CONFIG.URL_API + '/exemplar/:id/confirmar',
            method: 'POST'
        },
        alugar:{
            url: CONFIG.URL_API + '/exemplar/:id/alugar',
            method: 'POST'
        },
        devolver:{
            url: CONFIG.URL_API + '/exemplar/:id/devolver',
            method: 'POST'
        },
        reservados: {
            url:  CONFIG.URL_API + '/exemplar/reservados',
            method: 'GET', 
            isArray:true
        },
        alugados: {
            url:  CONFIG.URL_API + '/exemplar/alugados',
            method: 'GET', 
            isArray:true
        },
        emprestimos: {
            url:  CONFIG.URL_API + '/exemplar/emprestimos',
            method: 'GET', 
            isArray:true
        },
        disponiveis: {
            url:  CONFIG.URL_API + '/exemplar/disponiveis',
            method: 'GET', 
            isArray:true
        }

    })
}]).
factory('AuthService', function(){
    var self=this;
    return {
        getUser : function(){
            var user = {};
            user.token = sessionStorage.getItem('token');
            user.email = sessionStorage.getItem('email');
            user.nome = sessionStorage.getItem('nome');
            user.perfil = sessionStorage.getItem('perfil');
            console.log('gettando user', user)
            return user;
        },
        setUser : function(res){

            sessionStorage.setItem('token', res.token);
            sessionStorage.setItem('email', res.user.email);
            sessionStorage.setItem('nome', res.user.nome);
            sessionStorage.setItem('perfil',res.user.perfil);
        },
        isLoggedIn : function(){
            console.log('service funcao isLoggedIn')
            var token =  sessionStorage.getItem('token');
            return token && token != null 
        },
        clearCredentials: function(){
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('email');
            sessionStorage.removeItem('nome');
            sessionStorage.removeItem('perfil');
        }
    }
})
