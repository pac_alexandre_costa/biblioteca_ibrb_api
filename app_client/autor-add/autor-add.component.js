angular
.module('autorAdd')
.component('autorAdd', {
  templateUrl : 'autor-add/autor-add.template.html',
  controller:  ['AutorService', '$routeParams', '$location', function(AutorService, $routeParams, $location){
    var self = this;
    console.log('ja estou no autor add controller');

    self.insert = function(){
      console.log('funcao insert foi chamada')
      var autorService = new AutorService()
      autorService.nome = self.nome
      autorService.$save(
        (data)=>{
          $location.path('/autores').replace();
          console.log('deu certo a insercao de autor');
        }, 
        (err)=>{console.log('deu ruim na insercao do autor',err);self.msgErr = err.data.message;})
    }
  }]
})