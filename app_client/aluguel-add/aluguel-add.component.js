angular.
module('aluguelAdd').
component('aluguelAdd',{
  templateUrl : 'aluguel-add/aluguel-add.template.html',
  controller : [ '$http', '$location', 'AluguelService', 'LivroService', 'UserService', 'AuthService', 'ExemplarService', function AluguelController( $http, $location, AluguelService, LivroService, UserService, AuthService, ExemplarService){
    var self = this;
    self.aluguelPermission=false;
    if(AuthService.getUser().perfil==='admin'){    
      self.aluguelPermission=true;
    }
    else{
      self.aluguelPermission=false;
    }
    
    ExemplarService.disponiveis( 
      (data) => { self.livros = data;} , 
      (error) => {console.log('deu ruim ao recuperar os livros para aluguel',error);}
      )

    if (AuthService.getUser().perfil==='admin'){
      UserService.query(
        (data)=>{self.usuarios = data},
        (error) => {console.log('deu ruim ao recuperar os usuarios ....', error);}
        )
    }

    self.alugar = function(exemplar){
        ExemplarService.alugar({id:exemplar._id}, 
        (data)=>{console.log('deu certo alugar o livro');$location.path('/gerenciamento').replace();},
        (err)=>{console.log('deu ruim na reserva do exemplar', err)}
        )
    }

  }]
})