angular
	.module('home')
	// when defining a model, you need two parameters: name and definition
	// thee definition is compromised basically of the controller and the template url
	.component('home',{
		templateUrl: 'home/home.template.html',
		controller : ['$routeParams', 'AuthService', function($routeParams, AuthService){
			var self = this
			self.nome = AuthService.getUser().nome
		}] 
	})
