angular
.module('livroAdd')
.component('livroAdd', {
  templateUrl : 'livro-add/livro-add.template.html',
  controller: ['LivroService', 'AutorService', '$routeParams', '$location', function(LivroService, AutorService, $routeParams, $location){
    console.log('estou no controller component livro add')
    var self=this

    AutorService.query(function(data){
      self.autores = data;
      //$('#autor').select2();
    },
    function(error){
      console.log('deu ruim ao recuperar os autores '+ error)

    })

    self.insert = function(){
      console.log('metodo insert foi chamado', self.livro)

      var livroService = new LivroService()
      livroService.titulo = self.livro.titulo
      livroService.descricao = self.livro.descricao
      livroService.ano = self.livro.ano
      livroService.isbn = self.livro.isbn
      livroService.autor = self.livro.autor
      livroService.qtdExemplar = self.livro.qtdExemplar;
      livroService.capa = self.livro.capa;



      livroService.$save(function(data){
        console.log('what1', data)
        $location.path('/livros').replace()
      }, function(err){
        console.log('what2', err)
        self.msgErr = err.data.message
      })
      
      //.catch(function(req) { self.msgErr = req.data.message; console.log('deu ruim na insercao do livro')})
      //;;.finally(function()  { console.log('sempre é chamado') });

    }     
  }]
})