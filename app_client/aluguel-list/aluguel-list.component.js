angular.
module('aluguelList').
component('aluguelList', {
	templateUrl : 'aluguel-list/aluguel-list.template.html',
	controller :['$http', 'AuthService','LivroService','ExemplarService', function($http, AuthService, LivroService, ExemplarService){
		var self=this;

		self.devolver = function(exemplar){
			ExemplarService.devolver({id:exemplar._id}, 
				(data)=>{self.obterPedidosReserva();},
				(err)=>{console.log('deu ruim na devolucao do exemplar', err)}
				)
		};

	self.obterAlugueis=function(){
		ExemplarService.alugados(
			(data)=>{self.exemplares = data;},
			(error)=>{console.log('deu ruim ao recuperar os historicos', error);}
			);
	}


	console.log('estou no controllador do reserva-List');
	self.devolverPermission = false;
	if(AuthService.getUser().perfil==='admin'){
		self.devolverPermission = true;				
	}

	self.obterAlugueis();




}]
})