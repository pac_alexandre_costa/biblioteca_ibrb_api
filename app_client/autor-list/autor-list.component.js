angular
.module('autorList')
.component('autorList', {
  templateUrl : 'autor-list/autor-list.template.html',
  controller : ['AutorService', 'AuthService', function(AutorService, AuthService){
    var self = this

    self.viewPermission=true;
    self.addPermission=false;
    self.editPermission=false;
    if(AuthService.getUser().perfil==='admin'){
      self.addPermission=true;
      self.editPermission=true;
    }else if(AuthService.getUser().perfil==='usuario'){
      self.addPermission=false;
      self.editPermission=false;
    }

    
    console.log('esotu no controller de AutorService')
    AutorService.query(function(data){
      self.autores = data
      console.log('comedia', self.autores)
    },
    function(error){
      console.log('deu ruim ao recuperar os autores '+ error)

    })
  }]
})