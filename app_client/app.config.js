angular
.module('bibliotecaApp')
.factory('tokenInjector',  ['$q', '$location', 'AuthService', function($q, $location, AuthService){
  var tokenInjector = {
    request: function(config) {
      if(AuthService.isLoggedIn)
        config.headers['x-access-token'] = AuthService.getUser().token
      console.log('request interceptor na jogada '+ config.url)
      return config
    }
  }
  return tokenInjector
}]
)
.factory('responseInjector',  ['$q', function($q){
  var responseInjector = {
    response : function(response){
      console.log('response  ', response, response.status);
      return response
    }
  }
  return responseInjector
}]
)
.factory('responseErrorInjector',  ['$q', '$location', function($q, $location){
  var responseErrorInjector = {
    responseError : function(rejection){
      console.log('response error rejection ',  rejection);
      $location.path('/login').replace()
      return $q.reject(rejection)
    }
  }
  return responseErrorInjector
}]
)
.config(['$httpProvider', '$locationProvider','$routeProvider', function ($httpProvider, $locationProvider, $routeProvider){
  $httpProvider.interceptors.push('tokenInjector');
  $httpProvider.interceptors.push('responseInjector');
  //$httpProvider.interceptors.push('responseErrorInjector');



  $locationProvider.hashPrefix('!')
  console.log('estou no config')
  $routeProvider
// login e logout
  .when('/login',{
    template: '<login></login>',
    isAccessRequired : false,
  })
  .when('/logout',{
    template: '<logout></logout>',
    isAccessRequired : true,
    perfis: ['usuario','admin']
  })
// contas criar e alterar senha  
  .when('/signup',{
    template: '<signup></signup>',
    isAccessRequired : false
  })
  .when('/alterarSenha',{
    template: '<alterar-senha></alterar-senha>',
    isAccessRequired : false
  })
  .when('/usuarios',{
    template: '<usuario-list></usuario-list>',
    isAccessRequired : false
  })
  

// livros
  .when('/consulta',{
    template: '<consultaPublica></consultaPublica>',
    isAccessRequired : false,
  })
  .when('/livros',{
    template: '<livro-list></livro-list>',
    isAccessRequired : true,
    perfis: ['usuario','admin']
  })
  .when('/livro/new',{
    template: '<livro-add></livro-add>',
    isAccessRequired : true,
    perfis: ['admin']
  })
  .when('/livro/:livroId/view',{
    template: '<livro-detail></livro-detail>',
    isAccessRequired : true,
    perfis: ['usuario','admin']
  })
  .when('/livro/:livroId/edit',{
    template: '<livro-edit></livro-edit>',
    isAccessRequired : true,
    perfis: ['admin']
  })

// autores
  .when('/autores', {
    template: '<autor-list></autor-list>',
    isAccessRequired : true,
    perfis: ['usuario','admin']
  })
  .when('/autor/new', {
    template: '<autor-add></autor-add>',
    isAccessRequired : true,
    perfis: ['admin']
  })
  .when('/autor/:autorId/view', {
    template: '<autor-detail></autor-detail>',
    isAccessRequired : true,
    perfis: ['usuario','admin']
  })
  .when('/autor/:autorId/edit', {
    template: '<autor-edit></autor-edit>',
    isAccessRequired : true,
    perfis: ['admin']
  })

// alugueis
  .when('/alugueis',{
    template: '<aluguel-list></aluguel-list>',
    isAccessRequired : true,
    perfis: ['usuario','admin']
  })
  .when('/reservas',{
    template: '<reserva-list></reserva-list>',
    isAccessRequired:true,
    perfis: ['usuario','admin']
  })
  .when('/gerenciamento',{
    template: '<gerenciamento></gerenciamento>',
    isAccessRequired:true,
    perfis: ['admin']
  })

  .when('/aluguel/new',{
    template: '<aluguel-add></aluguel-add>',
    isAccessRequired : true,
    perfis: ['usuario','admin']
  })
  /*.when('/aluguel/:aluguelId/edit',{
    template: '<aluguel-edit></aluguel-edit>',
    isAccessRequired : true,
    perfis: ['usuario','admin']
  })*/

// demais
  .when('/home',{
    template: '<home></home>',
    isAccessRequired : true,
    perfis: ['usuario','admin']
  })
  .otherwise({
    template: '<login></login>',
    isAccessRequired : false
  })
}])
.run(['AuthService','$rootScope', '$location', function(AuthService, $rootScope, $location){
  $rootScope.$on('$routeChangeStart', function(event,next,current){
    if (AuthService.isLoggedIn()==false && next.isAccessRequired == true ){
      event.preventDefault()
      $location.path('/login').replace()
    }
    if (next.perfis && !next.perfis.includes(AuthService.getUser().perfil)){
     event.preventDefault();
     console.log('sem permissao');
     //$location.path('/login').replace() ;
   }
 })
}])
