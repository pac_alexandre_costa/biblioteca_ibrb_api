angular.
module('reservaList').
component('reservaList', {
	templateUrl : 'reserva-list/reserva-list.template.html',
	controller :['$http', 'AuthService','LivroService','ExemplarService', function($http, AuthService, LivroService, ExemplarService){
		var self=this;

		self.cancelar = function(exemplar){
			ExemplarService.cancelar({id:exemplar._id}, 
				(data)=>{self.obterPedidosReserva();},
				(err)=>{console.log('deu ruim na reserva do exemplar', err)}
				)
		};

	self.obterPedidosReserva=function(){
		ExemplarService.reservados(
			(data)=>{self.exemplares = data;},
			(error)=>{console.log('deu ruim ao recuperar os historicos', error);}
			);
	}


	console.log('estou no controllador do reserva-List');
	self.cancelarPermission = true;
	self.devolverPermission = false;
	if(AuthService.getUser().perfil==='admin'){
		self.devolverPermission = true;				
	}

	self.obterPedidosReserva();




}]
})