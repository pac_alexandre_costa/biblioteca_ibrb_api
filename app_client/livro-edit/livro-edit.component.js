angular
.module('livroEdit')
.component('livroEdit', {
	templateUrl : 'livro-edit/livro-edit.template.html',
	controller: ['LivroService', 'AutorService', '$routeParams', '$location', function(LivroService, AutorService, $routeParams, $location){
		console.log('estou no controller component livro add')
		var self=this

		LivroService.get({id: $routeParams.livroId}, 
			(dataLivro)=>{
				self.livro=dataLivro;
				console.log('comedia livro ', self.livro)

				AutorService.query(function(dataAutores){
					self.autores = dataAutores
					console.log('comedia',self.autores)
				},
				function(error){
					console.log('deu ruim ao recuperar os autores '+ error)
				})
			}, 
			(err)=>{console.log('deu ruim ao recuperar o livro');})

		

		self.update = function(){
			console.log('metodo insert foi chamado', self.livro)

			LivroService.update({id:self.livro.id}, 
				self.livro, 
				(data)=>{$location.path('/livros').replace();},
				(err)=>{self.msgErr = err.data.message; console.log('deu ruim na edicao do livro')})
		}     
	}]
})