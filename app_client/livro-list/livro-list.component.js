angular.
module('livroList').
component('livroList',{
	templateUrl : 'livro-list/livro-list.template.html',
	controller:[ 'LivroService' , 'AuthService', function (LivroService, AuthService){
		var self = this
		self.viewPermission=true;
		self.addPermission=false;
		self.editPermission=false;
		if(AuthService.getUser().perfil==='admin'){
			self.addPermission=true;
			self.editPermission=true;
		}else if(AuthService.getUser().perfil==='usuario'){
			self.addPermission=false;
			self.editPermission=false;
		}

		console.log('do something on LivroListController')
		LivroService.query(function(data){
			self.livros=data
		}, 
		function(error){
			console.log('deu ruim ao recuperar os livros '+ error)
		})
	}]
})