angular.
module('autorDetail').
component('autorDetail', {
  templateUrl: 'autor-detail/autor-detail.template.html',
  controller: ['AutorService', 'AuthService', '$routeParams', function(AutorService, AuthService, $routeParams){
    var self=this
    this.viewLivroPermission=false;
    this.editLivroPermission=false;
    if(AuthService.getUser().perfil==='admin'){
      this.viewLivroPermission=true;
     this.editLivroPermission=true;
   }else   if(AuthService.getUser().perfil==='usuario'){
    this.viewLivroPermission=true;
    this.editLivroPermission=false;
  }

  AutorService.get({id: $routeParams.autorId},
    (data)=>{self.autor = data;
      AutorService.livros({id: $routeParams.autorId}, 
        (data)=>{self.livros=data;},
        (err)=>{self.msgErr = err.data.message;}
        )

    }, 
    (err)=>{self.msgErr = err.data.message;});


}]
})