const https = require('https');
var request = require('request').defaults({ encoding: null });

module.exports = {
  getImage : function (url, callback) {
    request.get(url, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        data = new Buffer(body).toString('base64'); 
        callback(null,data)
      }else{
        callback(error,null)
      }
    })
  }
}
