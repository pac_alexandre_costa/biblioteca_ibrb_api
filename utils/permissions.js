module.exports = function(rolesAllowed) {
  return function(req, res, next) {
    // Implement the middleware function based on the options object
    //console.log('estou no middleware implementado', req.user)
    if (rolesAllowed.includes(req.user.local.perfil))
    	next();
    else
    	return res.status(403).json({ success: false, message: 'O usuario nao tem perfil de acesso suficiente.' }); 

  }
}