// route middleware to make sure a user is logged in
module.exports ={

	tryToRetrieveUser: function(req,res,next){
		var jwtSecret = require('../config/jwt');
		var jwt    = require('jsonwebtoken');

		var token = req.body.token || req.query.token || req.headers['x-access-token']
		req.user = null
		req.decoded = null
		if (token) {
			// decode token
			// verifies secret and checks exp
			jwt.verify(token, jwtSecret.secret, function(err, decoded) {
				if (!err){
					// if everything is good, save to request for use in other routes
					req.decoded = decoded
					req.user = decoded
					//console.log('decoded object ' +JSON.stringify(decoded) )
					next()
				}else{
					next()
				}
			})
		}else{
			next()
		}
	}
	,

	isLoggedIn: function (req, res, next) {

		var jwtSecret = require('../config/jwt');
		var jwt    = require('jsonwebtoken');

		// if user is authenticated in the session, carry on
		//if (req.isAuthenticated())
		// 	return next();

		// if they aren't redirect them to the home page
		//res.redirect('/');
		//}


		// check header or url parameters or post parameters for token
		var token = req.body.token || req.query.token || req.headers['x-access-token']


		if (token) {
			// decode token
			// verifies secret and checks exp
			jwt.verify(token, jwtSecret.secret, function(err, decoded) {
				if (err){
					return res.status(401).json({ success: false, message: 'Failed to authenticate token. ' + err });
				} else{ // if everything is good, save to request for use in other routes
					req.decoded = decoded
					req.user = decoded
					//console.log('decoded object ' +JSON.stringify(decoded) )
					next()
				}
			})
		}else{
			// if there is no token
			// return an error
			return res.status(403).send({
				success: false,
				message: 'No token provided.'
			})}
		}
	}
