var express = require('express');
var app = express();

var mongoose = require('mongoose');
var passport = require('passport');
//var flash    = require('connect-flash');

const cors = require('cors')

var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session  = require('express-session');

var path = require('path');
var favicon = require('serve-favicon');

var configDB = require('./config/database.js');
var loginUtils = require('./utils/login')
mongoose.connect(configDB.url);

require('./config/passport')(passport); // pass passport for configuration

app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


// required for passport
//app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
//app.use(passport.session()); // persistent login sessions
//app.use(flash()); // use connect-flash for flash messages stored in session

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(express.static(path.join(__dirname, 'app_client')));

// question : cors package is still necessary now that the angularjs app is in the same domain ?
app.use(cors());
app.options('*', cors());

app.get('/', (req,res)=>{
	res.sendfile('app_client/index.html')
})
app.use('/', express.Router().use(loginUtils.tryToRetrieveUser))

var account = require('./routes/account')(app, passport);
var user  = require('./routes/user');
var autor = require('./routes/autor');
var livro = require('./routes/livro');
var aluguel = require('./routes/aluguel');
var exemplar = require('./routes/exemplar');

app.use('/api/v1/account', account) // this route should be not protected because it will allow login and signup
//this will protect the following endpoints
//app.use('/api',express.Router().use(loginUtils.isLoggedIn))

app.use('/api/v1/user', user)
app.use('/api/v1/autor', autor);
app.use('/api/v1/livro', livro);
app.use('/api/v1/aluguel', aluguel);
app.use('/api/v1/exemplar', exemplar);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});




module.exports = app;
