var express = require('express');
var router = express.Router();

var Aluguel = require('../models/aluguel')
var Livro = require('../models/livro')
var Exemplar = require('../models/exemplar')
var Autor = require('../models/autor')

var permissions = require('../utils/permissions')
var loginUtils = require('../utils/login')

console.log('estou em routes/exemplar.js')

/* GET home page. */
// comentado temporariamente apenas para testar a chamada
//router.get('/', function(req, res) {


 router.get('/emprestimos', express.Router().use(loginUtils.isLoggedIn),permissions(['admin']), function(req,res){
  Exemplar.find({ status : { $in: ['Alugado','Reservado'] } })
  .populate('livro')
  .populate('autor')
  .populate('locador local.email local.nome')
  .populate('locatario local.email local.nome')
  .exec(function(errExemplar, exemplares){

    if(errExemplar){
      console.log('-----------------')
      console.log(errExemplar);
      res.status(400).send({message:'erro ao recuperar os emprestimos de exemplares', err:errExemplar});
      return;
    }
    res.status(200).send(exemplares);
    return;
  })
});

 router.get('/disponiveis', express.Router().use(loginUtils.isLoggedIn),permissions(['admin']), function(req,res){
  Exemplar.find({ status : { $in: ['Disponivel'] } })
  .populate('livro')
  .populate('autor')
  .populate('locador local.email local.nome')
  .populate('locatario local.email local.nome')
  .exec(function(errExemplar, exemplares){

    if(errExemplar){
      console.log('-----------------')
      console.log(errExemplar);
      res.status(400).send({message:'erro ao recuperar os exemplares disponiveis', err:errExemplar});
      return;
    }
    res.status(200).send(exemplares);
    return;
  })
});


 router.get('/:status', express.Router().use(loginUtils.isLoggedIn),permissions(['usuario','admin']), function(req,res){
  var filtro;
  if(req.params.status==='reservados'){
    filtro='Reservado';
  }else if(req.params.status==='alugados'){
    filtro='Alugado'
  }

  Exemplar.find({ status : filtro , locatario : req.user._id})
  .populate('livro')
  .populate('autor')
  .populate('locador local.email local.nome')
  .populate('locatario local.email local.nome')
  .exec(function(errExemplar, exemplares){

    if(errExemplar){
      res.status(400).send({message:'erro ao recuperar os exemplares', err:errExemplar});
      return;
    }

    res.status(200).send(exemplares);
    return;
  })
});

 router.post('/:exemplar_id/reservar', express.Router().use(loginUtils.isLoggedIn),permissions(['usuario','admin']), function(req, res) {
  Exemplar.findById( req.params.exemplar_id).exec(function(err, exemplar){
    if(err){
      res.status(400).send({message:'Não foi possível encontrar o exemploar para reservar.', err:err})
      return;
    }
    if(!exemplar){
      res.status(400).send({message:'Nao foi possivel encontrar o exemplar'});
      return;
    }

    if(exemplar.status==='Disponivel'){
      exemplar.status='Reservado';
      exemplar.dataReserva = new Date();
      exemplar.locatario = req.user._id;
      exemplar.save(function(err, exemplarAtualizado){
        if(err){
          res.status(400).send('Nao foi possivel reservar este exemplar ' + err);
          return;
        }
        res.status(200).send(exemplarAtualizado);
      })
    }
    else{
      res.status(400).send('O exemplar nao se encontra disponivel para reserva.' + err);
      return;
    }
  })
})




 router.post('/:exemplar_id/cancelar', express.Router().use(loginUtils.isLoggedIn),permissions(['admin', 'usuario']), function(req, res) {
  Exemplar.findById( req.params.exemplar_id).exec(function(err, exemplar){
    if(err){
      res.status(400).send({message:'Não foi possível encontrar o exemploar para cancelar.'})
      return;
    }
    if(exemplar.status==='Reservado' && (exemplar.locatario==req.user._id || req.user.local.perfil==='admin')){

      var aluguel = new Aluguel({
        locatario : exemplar.locatario,
        locador : exemplar.locador,
        dataReserva : exemplar.dataReserva,
        dataCancelamento :  new Date(),
        dataAluguel : exemplar.dataAluguel,
        dataDevolucao : exemplar.dataDevolucao,
        status: 'Reserva Cancelada'
      })

      aluguel.save(function(errHistorico, historicoInserido){
        if(errHistorico){
          res.status(400).send('Nao foi possivel registrar o historico', errHistorico);
          return;
        }

        exemplar.status='Disponivel';
        exemplar.locador = undefined;
        exemplar.locatario = undefined;
        exemplar.dataReserva = undefined;
        exemplar.dataCancelamento = undefined;
        exemplar.dataAluguel = undefined;
        exemplar.dataDevolucao = undefined;
        exemplar.historico.push(historicoInserido);

        exemplar.save(function(err, exemplarAtualizado){
          if(err){
            res.status(400).send('Nao foi possivel cancelar esta reserva deste exemplar ' + err);
            return;
          }
          res.status(200).send(exemplarAtualizado);
        })
      })


    }
    else{
      res.status(400).send('O exemplo nao se encontra reservado para ser cancelado. ' + err);
      return;
    }
  })
})



 router.post('/:exemplar_id/alugar', express.Router().use(loginUtils.isLoggedIn),permissions(['admin']), function(req, res) {
  debugger;
  Exemplar.findById( req.params.exemplar_id).exec(function(err, exemplar){
    console.log('exemplar encontrado ')
    if(err){
      res.status(400).send({message:'Não foi possível encontrar o exemploar para alugar.'})
      return;
    }
    if(exemplar.status==='Disponivel'){
      exemplar.status='Alugado';
      exemplar.dataAluguel = new Date();
      exemplar.locatario = req.user._id;
      exemplar.save(function(err, exemplarAtualizado){
        if(err){
          res.status(400).send('Nao foi possivel alugar exemplar ' + err);
          return;
        }
        res.status(200).send(exemplarAtualizado);
      })
    }
    else{
      res.status(400).send('O exemplar esta reservado ou alugado para outra pessoa.' + err);
      return;
    }
  })
})


 router.post('/:exemplar_id/confirmar', express.Router().use(loginUtils.isLoggedIn),permissions(['admin']), function(req, res) {
  debugger;
  Exemplar.findById( req.params.exemplar_id).exec(function(err, exemplar){
    console.log('exemplar encontrado ')
    if(err){
      res.status(400).send({message:'Não foi possível encontrar o exemploar para alugar.'})
      return;
    }
    if(exemplar.status==='Reservado'){
      exemplar.status='Alugado';
      exemplar.dataAluguel = new Date();
      exemplar.save(function(err, exemplarAtualizado){
        if(err){
          res.status(400).send('Nao foi possivel alugar exemplar ' + err);
          return;
        }
        res.status(200).send(exemplarAtualizado);
      })
    }
    else{
      res.status(400).send('O exemplar esta reservado ou alugado para outra pessoa.' + err);
      return;
    }
  })
})

 router.post('/:exemplar_id/devolver', express.Router().use(loginUtils.isLoggedIn),permissions(['admin']), function(req, res) {
  Exemplar.findById( req.params.exemplar_id).exec(function(err, exemplar){
    if(err){
      res.status(400).send({message:'Não foi possível encontrar o exemploar para devolver.'})
      return;
    }
    if(exemplar.status==='Alugado'){

      var aluguel = new Aluguel({
        locatario : exemplar.locatario,
        locador : exemplar.locador,
        dataReserva : exemplar.dataReserva,
        dataCancelamento : exemplar.dataCancelamento,
        dataAluguel : exemplar.dataAluguel,
        dataDevolucao :  new Date(),
        status: 'Devolvido'
      })


      aluguel.save(function(errHistorico, historicoInserido){
        if(errHistorico){
          res.status(400).send('Nao foi possivel registrar o historico', errHistorico);
          return;
        }
        exemplar.status='Disponivel';

        exemplar.status='Disponivel';
        exemplar.locador = undefined;
        exemplar.locatario = undefined;
        exemplar.dataReserva = undefined;
        exemplar.dataCancelamento = undefined;
        exemplar.dataAluguel = undefined;
        exemplar.dataDevolucao = undefined;
        exemplar.historico.push(historicoInserido);

        exemplar.save(function(err, exemplarAtualizado){
          if(err){
            res.status(400).send('Nao foi possivel devolver este exemplar ' + err);
            return;
          }
          res.status(200).send(exemplarAtualizado);
        })

      })

    }
    else{
      res.status(400).send('O exemplar nao esta alugado para ser devolvido.' + err);
      return;
    }
  })
})




 module.exports = router;
