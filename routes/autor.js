var express = require('express');
var router = express.Router();

var Autor = require('../models/autor')
var Livro = require('../models/livro')


var loginUtils = require('../utils/login')

var permissions = require('../utils/permissions')

router.get('/',  function(req, res) {
	Autor.find( {},  null, {sort:'nome'}, function(err, autores){
		if(!err)
			res.json(autores)
		else
			console.log(err)
	})
})

router.get('/:autor_id',function(req, res) {
	Autor.findById( req.params.autor_id, function(err, autor){
		if(err){
			console.log(err);
			res.status(400).send({message:'Erro ao consultar o autor.', err:err});
			return;
		}
		if(!autor){
			res.status(400).send({message:'Autor'+ req.params.autor_id + 'inexiste na base.'});
			return;
		}
		res.status(200).json(autor)
	})
})

router.get('/:autor_id/livros', function(req, res) {
	Livro.find({autor:req.params.autor_id}).populate('autor').exec(function(err, livros){
		if(!err){
			// todo : adicionar o campo disponivel dinamicamente
			res.json(livros);
		}
		else{
			res.status(400).send({message:'Não foi possível consultar os livros por autor', err:err})
		}
	})
})


router.post('/', express.Router().use(loginUtils.isLoggedIn),permissions(['admin']),function(req, res) {
	var novoAutor = new Autor({nome:req.body.nome});
	novoAutor.save(function (err) {
		if (err)
			res.status(400).send({message:'Não foi possível inserir um autor:'+err})
		else
			res.status(200).send({message:'Autor inserido com sucesso.'})
	})
})

router.put('/', express.Router().use(loginUtils.isLoggedIn),permissions(['admin']),function(req,res){
	console.log('put chamado para autor ', JSON.stringify(req.body))
	Autor.findByIdAndUpdate( req.body._id,  { $set: { nome:req.body.nome} }, {new: true}, function (err, doc) {
		if(err){
			console.log(err)
			res.status(400).send({message:'Não foi possível atualizar o autor: '+err})
		}else{
			console.log('sucesso? ', doc)
			res.status(200).send({message:'Autor atualizado com sucesso.'})
		}

	})
})

// o id do autor a ser removido é passado na url pois o req.body está vindo como indefinido
router.delete('/:autor_id', express.Router().use(loginUtils.isLoggedIn),permissions(['admin']), function(req,res){

	console.log('req.params.autor_id '+ req.params.autor_id)

	Livro.count({ autor : req.params.autor_id }, function (err, count){
		console.log('count '+count)
		if(count==0){
			Autor.remove( { _id : req.params.autor_id }, function (err) {
				if(err){
					console.log(err)
					res.status(400).send({message:'Não foi remover o autor'})
				}else{
					res.status(200).send({message:'Autor removido com sucesso.'})
				}

			})}else {
				res.status(400).send({message:'Este autor possui livros associados e não pode ser removido'})
			}
		});
})


module.exports = router;
