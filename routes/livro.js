var express = require('express');
var shorthash = require('shorthash');
var router = express.Router();

var loginUtils = require('../utils/login')
var fileUtils = require('../utils/file')

var Livro = require('../models/livro')
var Exemplar = require('../models/exemplar')
var Autor = require('../models/autor')
var User = require('../models/user')

var permissions = require('../utils/permissions')

console.log('estou em routes/livro.js')

/* GET home page. */
// comentado temporariamente apenas para testar a chamada
//router.get('/', function(req, res) {
	router.get('/',  function(req, res) {
		var queryObj = {}
		if( req.query.queryStr){
			queryObj = {$text:{$search:req.query.queryStr}}
		//	queryObj = {"titulo" : { "$regex": req.params.queryStr, "$options":"i"}}
		}
		Livro.find(queryObj).sort('titulo').populate('autor').lean().exec(function(err, livros){
			if(!err){
				var userId
				if(req.user && req.user._id)
					userId = req.user._id
				User
					.findById(userId)
					.populate('favoritos')
					.exec(function(errUser, resultUser){
						if(!errUser && resultUser){
							var favoritosId = resultUser.favoritos.map(fav=>fav._id.toString())
							livros.forEach((l)=>{
								l.favorito = favoritosId.includes(l._id.toString())})
						}
						res.json(livros);
					})
			}
			else{
				res.status(400).send({message:'Não foi possível consultar os livros'})
			}
		})
	})

	router.get('/top10',  function(req, res) {
		Livro.find({'visualizacoes':{$ne:0, $exists: true}}).sort({visualizacoes:-1}).limit(10).populate('autor').exec(function(err, livros){
			if(!err){
			// todo : adicionar o campo disponivel dinamicamente
			res.json(livros);
		}
		else{
			res.status(400).send({message:'Não foi possível consultar os livros'})
		}
	})
	})

	router.get('/:livro_id', function(req, res) {
		Livro.findByIdAndUpdate(
			req.params.livro_id
			,  {$inc: { visualizacoes: 1} }
			, function(errDocLivro,docLivro){

			}
			)
		Livro.findById( req.params.livro_id).lean().exec(function(err, livro){
			if(err){
				res.status(400).send({message:'Não foi possível consultar este livro específico.', err:err})
				return;
			}
			if(!livro){
				res.status(400).send({message:'Livro '+req.params.livro_id+' inexiste na base.'});
				return;
			}

			Exemplar.find({livro:livro._id}).exec(function(errExemplar, exemplares){
				if(errExemplar){
					res.status(400).send({message:'Nao foi possivel obter os exemplares deste livro', err:errExemplar});
					return;
				}
				livro.exemplar = exemplares;
				res.status(200).json(livro);
				return;
			})
		})
	})


	router.post('/', express.Router().use(loginUtils.isLoggedIn), permissions(['admin']), function(req, res) {
		var i = 0;
		var novoLivro = new Livro({nome:req.body.nome, isbn: req.body.isbn, ano:req.body.ano, titulo:req.body.titulo, capa:req.body.capa})
		console.log('req.body.autor ' +req.body.autor)

		fileUtils.getImage(novoLivro.capa, (errGetImage, dataGetImage)=>{
			if(errGetImage){
				res.status(400).send( 'Imagem/Url inválida' )
				return
			}
			Autor.find()
			.where("_id").in(req.body.autor)
			.exec(function(err, autores){
				if(err){
					res.status(400).send(err)
					return;
				}
				if(autores[0]==undefined){
					res.status(400).send( 'Autores Invalidos' )
					return;
				}

				novoLivro.imagem= dataGetImage
				novoLivro.autor = autores
				novoLivro.save(function (err) {
					if (err) {
						console.log(err)
						res.status(400).send( { message: (err.name === 'MongoError' && err.code === 11000) ? 'ISBN já utilizado !' : errorHandler.getErrorMessage(err) } )
						return;
					}

					var exemplaresAr = [];
					for  (i=0;i<req.body.qtdExemplar;i++){
						var exemplar = new Exemplar({});
						exemplar.livro=novoLivro;
						exemplar.status='Disponivel';
						exemplar.patrimonio = shorthash.unique(novoLivro.titulo+i+new Date());
					//console.log(exemplar);
					exemplaresAr.push(exemplar);
				}

				Exemplar.collection.insert(exemplaresAr, function(errExemplar, exemplaresDoc){
					//	console.log('exemplares DOc', exemplaresDoc);
					if(errExemplar){
						res.status(400).send({message:'Nao foi possivel inserir o exemplar deste livro: ', err:errExemplar});
						return;
					}
					res.status(200).send({success : 'true', data: novoLivro});
					return;
				})
			})
			})
		})
	})

	router.put('/', express.Router().use(loginUtils.isLoggedIn),permissions(['admin']), function(req,res){
		fileUtils.getImage(req.body.capa, (errGetImage, dataGetImage)=>{
			if(errGetImage){
				res.status(400).send( 'Imagem/Url inválida' )
				return
			}
			Livro.findByIdAndUpdate(
				req.body._id,
				{
					$set : {
						isbn: req.body.isbn,
						titulo:req.body.titulo,
						descricao :req.body.descricao,
						ano: req.body.ano,
						autor: req.body.autor,
						capa: req.body.capa,
						imagem: dataGetImage
					}
				},
				{
					upsert : false,
					new : true,
					runvalidators:true
				},
				function (err, doc){
					if(err){
						res.status(400).send( { message: (err.name === 'MongoError' && err.code === 11000) ? 'ISBN já utilizado !' : errorHandler.getErrorMessage(err) } )
					}else if (doc==undefined){
						res.status(400).send({message:'Livro nao encontrado para ser atualizado', err:err})

					}
					else{
						res.status(200).send({message:'Livro Atualizado com sucesso', data:doc})
					}
				}
				)
		})
	})


	module.exports = router;
