var express = require('express');
var router = express.Router();

var User = require('../models/user')
var loginUtils = require('../utils/login')
var Autor = require('../models/autor')
var Livro = require('../models/livro')
var permissions = require('../utils/permissions')

router.get(
  '/'
  , express.Router().use(loginUtils.isLoggedIn)
  , permissions(['admin'])
  , function(req,res){
    User.find({}, 'local.email local.nome local.perfil',
    function(err, users){
      if(!err)
      res.json(users)
      else
      console.log(err)
    }
  )
})

router.get(
  '/me'
  , express.Router().use(loginUtils.isLoggedIn)
  , permissions(['usuario','admin'])
  , function(req,res){
    User.findById(req.user._id, 'local.email local.nome local.perfil',
    function(err, user){
      if(!err)
      res.status(200).json(user)
      else
      res.status(400).send();
    }
  )
})



router.post(
  '/updatePassword'
  , express.Router().use(loginUtils.isLoggedIn)
  , permissions(['usuario','admin'])
  , function(req,res){
    User.findByIdAndUpdate(
      req.user._id,
      {
        $set :{
          'local.password' :  new User().generateHash(req.body.password)
        }
      },
      {
        upsert : false,
        new : true,
        runvalidators:true
      },
      function (err, user){
        if(err){
          res.status(400).send({message:'Nao foi possivel atualizar o cadastro do usuario ', err:err})
        }else if (user==undefined){
          res.status(400).send({message:'Usuario nao encontrado para ser atualizado', err:err})
        }
        else{
          res.status(200).send()
        }
      })
    }
  )

  router.get(
    '/favoritos'
    , express.Router().use(loginUtils.isLoggedIn)
    , permissions(['usuario','admin'])
    , function(req,res){
      User
      .findById(req.user._id)
      .exec(function(err,usuario){
        if(err){
          res.status(400).send({message:'Impossivel recuperar os favoritos.', err:err})
        }else {
          Livro
          .find({'_id':{$in:usuario.favoritos}})
          .populate('autor')
          .exec(function(errLivro, livros){
            if(errLivro)
            res.status(400).send({message:'Impossivel recuperar os favoritos.', err:err})
            else
            res.status(200).json(livros)
          }
        )}
      })
    }
  )

  router.put(
    '/favoritos/:op'
    , express.Router().use(loginUtils.isLoggedIn)
    , permissions(['usuario','admin'])
    , function(req,res){
      Livro.findById(req.body.livroId).populate('autor').lean().exec(function(err, livroResult){
        if(err){
          res.status(400).send({message:'Erro ao pesquisar o livro'+req.body.livroId, err:err})
          return
        }
        if(!livroResult){
          res.status(400).send({message:'Livro não encontrado:'+req.body.livroId, err:err})
          return
        }
        var modificacao;
        if(req.params.op==='add'){
          modificacao =  {$addToSet : { favoritos : livroResult._id}}
        }else if(req.params.op==='remove'){
          modificacao ={$pull : { favoritos : livroResult._id}}
        }
        User.findByIdAndUpdate(
          req.user._id
          , modificacao
          , {new:true, upsert:false}
          , function(errUser, userResult){
            if(errUser){
              res.status(400).send({message:'Erro ao atualizar os favoritos do usuario', err:err})
              return
            }
            if(!userResult){
              res.status(400).send({message:'Usuário inexistente', err:err})
              return
            }
            	var favoritosId = userResult.favoritos.map(fav=>fav._id.toString())
              livroResult.favorito = favoritosId.includes(livroResult._id.toString())
            res.status(200).send(livroResult)
          }
        )
      })
    }
  )





  module.exports = router;
