var loginUtils = require('../utils/login')
// app/routes.js
module.exports = function(app, passport) {

    var express = require('express')
    var router = express.Router()

    var User = require('../models/user')

    var jwt    = require('jsonwebtoken') // used to create, sign, and verify tokens
    var jwtSecret = require('../config/jwt.js')

    
    
    // =====================================
    // HOME PAGE (with login links) ========
    // =====================================
    // app.get('/', function(req, res) {
    //     res.render('index.ejs'); // load the index.ejs file
    // });

    // =====================================
    // LOGIN ===============================
    // =====================================
    // show the login form
    // app.get('/login', function(req, res) {

    //     // render the page and pass in any flash data if it exists
    //     res.render('login.ejs')
    // });

    // process the login form
    // app.post('/login', do all our passport stuff here);
    // process the login form
    
    /*app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/home', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));*/

    router.post('/login', function (req,res,next){

        passport.authenticate('local-login' , {session : false, failureFlash:false} , function(err, user, info){
            console.log('estou na mensagem de callback ' + err + '--' +user)
            if(err){
                console.log('if err ....'+info)
                res.status(400).send(info)
            }
            else if(!user){
                console.log('if !user ....'+info)
                res.status(400).send(info)
            }else {
                console.log('parece que deu certo')
                console.log('req.user object ' + user)

/*
                var userInfo = {
                    nome: user.local.nome,
                    email: user.local.email,
                    perfil : user.local.perfil
                }*/

                
                user.local.password = undefined;
                user.favoritos = undefined

                var token = jwt.sign(user.toObject(), jwtSecret.secret, {expiresIn: 120 * 24})
                res.json({success:true, message:'Enjoy your tokens', token:token, user:user.local})
            }
        })(req, res, next)
    })

    // =====================================
    // SIGNUP ==============================
    // =====================================
    // show the signup form
    // app.get('/signup', function(req, res) {

    //     // render the page and pass in any flash data if it exists
    //     res.render('signup.ejs', { message: req.flash('signupMessage') });
    // });

    // process the signup form
    // app.post('/signup', do all our passport stuff here);
    // process the signup form
    router.post('/signup', function(req,res,next){
        passport.authenticate('local-signup' , {session : false, failureFlash:false} , function(err, user, info){
            console.log('estou na mensagem de callback ' + err + '--' +user)
            if(err){
                console.log('if err ....'+info)
                res.status(400).send({message:info.message, err:info})
            }
            else if(!user){
                console.log('if !user ....'+info)
                res.status(400).send({message:info.message, err:info})
            }else {
                console.log('parece que deu certo')
                console.log('req.user object ' + user)
                user.local.password = undefined;
                user.favoritos = undefined
                var token = jwt.sign(user.toObject(), jwtSecret.secret, {expiresIn: 60 * 24})
                res.json({success:true, message:'Enjoy your account', token:token})
            }
        })(req, res, next)
    })


    



    // =====================================
    // PROFILE SECTION =====================
    // =====================================
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)
    // app.get('/profile', isLoggedIn, function(req, res) {
    //     res.render('profile.ejs', {
    //         user : req.user // get the user out of session and pass to template
    //     });
    // });

    // // =====================================
    // // LOGOUT ==============================
    // // =====================================
    // app.get('/logout', function(req, res) {
    //     req.logout();
    //     res.redirect('/');
    // });


    // app.get('/home', isLoggedIn, function(req,res){
    //     res.render('home.ejs');
    // })
    return router
};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}


