var express = require('express');
var router = express.Router();
var mongoose = require('mongoose')
var Livro = require('../models/livro')
var Autor = require('../models/autor')
var Aluguel = require('../models/aluguel')
var permissions = require('../utils/permissions')

var loginUtils = require('../utils/login')

console.log('estou em routes/aluguel.js')

/* GET home page. */
router.get('/', express.Router().use(loginUtils.isLoggedIn),permissions(['usuario','admin']), function(req, res) {
  var query = {};
  if(!req.user.local.perfil.includes('admin')){
    query.locatario = req.user._id;		
  }
  Aluguel.find(query).populate('livro').populate('locador', 'local.email').populate('locatario', 'local.email').exec(function(err, alugueis){
    if(err){
      console.log(err)
      res.status(400).send({message:'Não foi possível consultar os alugueis'})
    }
    else{
      res.json(alugueis)
    }
  })
})

router.get('/:aluguel_id', express.Router().use(loginUtils.isLoggedIn),permissions(['usuario','admin']), function(req,res){
  Aluguel.findById(req.params.aluguel_id).	
  exec(function(err, aluguel){
    if (err){
      console.log('Não foi possível consultar um aluguel',err);
      res.status(400).send({message:'Não foi possível consultar um aluguel'});
    }
    else
      res.status(200).json(aluguel)
  })

})



router.post('/:acao', express.Router().use(loginUtils.isLoggedIn),permissions(['usuario','admin']), function(req,res){
  console.log('req.body._id = '+req.body._id)
  Aluguel.findById( req.body._id, function(errFind, aluguel){
    if(errFind){
      res.status(400).send('Nao foi possivel atualizar a reserva/o aluguel deste livro ' + errFind)
      return;
    }

    if(req.params.acao === 'cancelar' ){
      if(aluguel==undefined){
        res.status(400).send('Reserva inexistente')
        return;
      }
      if(aluguel.status!=='Reservado'){
        res.status(400).send('Só é possível cancelar reservas')
        return;
      }
    }

    if( req.params.acao === 'devolver'){
      if(aluguel==undefined){
        res.status(400).send('Aluguel inexistente')
        return;
      }
      if(aluguel.status!=='Alugado'){
        res.status(400).send('Só é possível devolver alugueis')
        return;
      }
    }


    if (aluguel == undefined ){
      console.log('aluguel nao encontrado ainda')
      aluguel = new Aluguel({
        livro:req.body.livro
      })
    }else{
      console.log('objeto aluguel encontrado '+ aluguel)
    }


    switch(req.params.acao)
    {
      case 'reservar':
      console.log('1')
      aluguel.status ='Reservado'
      aluguel.locatario = req.user._id;
      aluguel.dataReserva = new Date()
      break
      case 'alugar':
      console.log('2')
      aluguel.status ='Alugado'
      aluguel.locador=req.user._id;
      aluguel.locatario=req.body.locatario;
      aluguel.dataAluguel = req.body.dataAluguel && req.body.dataAluguel!=null ? req.body.dataAluguel : new Date();
      break
      case 'cancelar':
      console.log('3')
      aluguel.status ='Reserva Cancelada'
      aluguel.dataCancelamento = new Date()
      break
      case 'devolver':
      console.log('4')
      aluguel.status ='Devolvido'
      aluguel.dataDevolucao = req.body.dataDevolucao && req.body.dataDevolucao!=null ? req.body.dataDevolucao : new Date();
      break
      default:
      console.log('5')
    }

    aluguel.alugar()


    var query = {};
    query.livro = aluguel.livro;

    if(req.params.acao === 'alugar'){
      // verifica se ha reservas ou se esta alugado
      query['$or'] = [{status : 'Reservado'}, {status : 'Alugado'}]
    }else if(req.params.acao === 'reservar'){
      // verifica se ha reservas
      query.status = 'Reservado';
      query.locatario = { $ne : aluguel.locatario};
    }
    Aluguel.count(query, function(errCount, count){
      if(errCount){
        res.status(400).send('Nao foi possivel atualizar o aluguel deste livro ' + errCount)
        return;
      }
      if(count != 0 ){
        res.status(400).send('Livro ja se encontrado reservado/alugado');
        return;
      }
      Aluguel.findByIdAndUpdate(
        aluguel._id,
        { $set: 
          { 
            dataReserva : aluguel.dataReserva,
            dataCancelamento : aluguel.dataCancelamento,
            dataAluguel: aluguel.dataAluguel,
            dataDevolucao : aluguel.dataDevolucao,
            livro :aluguel.livro,
            locador : aluguel.locador,
            locatario : aluguel.locatario,
            status : aluguel.status

          }
        },
        { upsert : true, new : true	, runValidators : true},
        function(errUpd, doc){
          if(errUpd){
            res.status(400).send('Nao foi possivel atualizar o aluguel deste livro ' + errUpd)
          }else{
            res.status(200).send(doc)
          }
        })
    })
  })
})

module.exports = router;
