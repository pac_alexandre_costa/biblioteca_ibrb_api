var gulp = require('gulp');
var gulpNgConfig = require('gulp-ng-config');

gulp.task('development', function () {
  gulp.src('app-config.json')
  .pipe(gulpNgConfig('bibliotecaAppConfig',{
      environment: 'development' // or maybe use process.env.NODE_ENV here
    }))
  .pipe(gulp.dest('./app_client/config'))
})

gulp.task('production', function () {
  gulp.src('app-config.json')
  .pipe(gulpNgConfig('bibliotecaAppConfig',{
      environment: 'production' // or maybe use process.env.NODE_ENV here
    }))
  .pipe(gulp.dest('./app_client/config'))
})